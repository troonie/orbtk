use crate::prelude::*;

property!(
    /// `GridRow` describes the row position of a widget on the `Grid`.
    GridRow(usize)
);

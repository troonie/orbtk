use crate::prelude::*;

property!(
    /// `ColumnSpan` describes the column span of a widget on the `Grid`.
    ColumnSpan(usize)
);

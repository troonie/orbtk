use crate::prelude::*;

#[test]
fn test_into() {
    let opacity: Opacity = 20.0.into();
    assert_eq!(opacity.0, 20.0);
}

use crate::{prelude::*, utils::*};

#[test]
fn test_into() {
    let border_brush: BorderBrush = "#000000".into();
    assert_eq!(border_brush.0, Brush::SolidColor(Color::rgb(0, 0, 0)));

    let border_brush: BorderBrush = "#ffffff".into();
    assert_eq!(border_brush.0, Brush::SolidColor(Color::rgb(255, 255, 255)));
}

use crate::prelude::*;

property!(
    /// `Focused` describes the focused (keyboard focus) state of a widget.
    Focused(bool)
);
